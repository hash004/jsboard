require 'net/http'
require 'net/https'
require 'json'
require 'uri'
require 'pp'

def get_cat()
  current_time = DateTime.now
  current_time = current_time.strftime "%H%M%S"

  catgif = "http://thecatapi.com/api/images/get?format=src&type=gif&api_key=NDEzNzA&size=small&date=" + current_time

  return catgif
end

def get_joke()
  uri              = URI.parse("http://api.icndb.com")
  http             = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl     = false
  http.verify_mode = OpenSSL::SSL::VERIFY_NONE
  req              = Net::HTTP::Get.new("/jokes/random?firstName=JSDev&lastName=Team")

  response         = http.request(req)
  return JSON.parse(response.body)
end

def get_status(status)

  if status == "Waiting+for+Support"
    status = "%27#{status}%27+AND+labels=JS-Dev"
  else
    status = "%27#{status}%27"
  end

  uri              = URI.parse("https://flashtalkingus.atlassian.net")
  http             = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl     = true
  http.verify_mode = OpenSSL::SSL::VERIFY_NONE
  req              = Net::HTTP::Get.new("/rest/api/2/search?jql=project+in+(TIT,HTMLC,JST,JSDEV,DIGM,JDS)+AND+status=#{status}&fields=*none&maxResults=0")

  req.basic_auth "jsdev", "flashtalking"

  response         = http.request(req)
  return JSON.parse(response.body)
end

# :first_in sets how long it takes before the job is first run. In this case, it is run immediately
SCHEDULER.every '10s', :first_in => 0 do |job|
  waitingsupport = get_status "Waiting+for+Support"
  indev = get_status "In+development"
  devdone = get_status "Dev+done"
  inqa = get_status "In+QA"

  send_event('waitingsupport', { text: waitingsupport["total"]})
  send_event('indev', { text: indev["total"]})
  send_event('devdone', { text: devdone["total"]})
  send_event('inqa', { text: inqa["total"]})

end

# :first_in sets how long it takes before the job is first run. In this case, it is run immediately
SCHEDULER.every '30s', :first_in => 0 do |job|
  catgif = get_cat
  jokes = get_joke
  jokes["value"]["joke"].gsub! /(\W)he(\W)/i, '\1they\2'
  jokes["value"]["joke"].gsub! /(\W)his(\W)/i, '\1their\2'
  jokes["value"]["joke"].gsub! /(\W)him(\W)/i, '\1them\2'
  jokes["value"]["joke"].gsub! "JSDev Team is", "JSDev Team are"
  jokes["value"]["joke"].gsub! "JSDev Team has", "JSDev Team have"
  jokes["value"]["joke"].gsub! "JSDev Team' ", "JSDev Team's "
  jokes["value"]["joke"].gsub! "&quot;", "'"

  send_event('cats', { image: catgif})
  send_event('jokes', { text: jokes["value"]["joke"]})

end